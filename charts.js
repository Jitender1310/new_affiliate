var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Advertisers', 'Clicks', 'Singups', 'FTD', 'Signup to deposit', 'Profit'],
        datasets: [{
            label: 'Advertisers',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1,
            responsive: true
        }
    ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
var ctxPi = document.getElementById('pieChart').getContext('2d');
// Pie chart emample...ctx.
var myPieChart = new Chart(ctxPi, {
    type: 'doughnut',
    data: {
        labels: ["Advertisers", "Clicks", "Signups", "FTD", "STD", "Profit"],
        datasets: [{
            label: false,
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850", "#efefef"],
          data: [2478,5267,734,784,433, 599]
        }]
      },
    options: {
        title: {
          display: true,
          text: 'Past month stats'
        },
        responsive: true,
        maintainAspectRatio: false,
        
      }
});